package az.com.main.service;

import az.com.main.domain.Classes;
import az.com.main.domain.Record;
import az.com.main.domain.Teacher;
import az.com.main.dto.*;
import az.com.main.enums.Status;
import az.com.main.exception.InvalidRequestException;
import az.com.main.exception.NotFoundException;
import az.com.main.repository.ClassRepository;
import az.com.main.repository.RecordRepository;
import az.com.main.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonService {

    private final RecordRepository recordRepository;
    private final TeacherRepository teacherRepository;
    private final ClassRepository classRepository;

    public ResponseEntity<Set<ClassDto>> getClassesByDateAndTeacher(RequestDto requestDto) {
        ResponseEntity<Set<ClassDto>> response = null;
        if (requestDto.getFrom() == null || requestDto.getTo() == null || requestDto.getTeacherId() == null) {
            throw new InvalidRequestException("Invalid Request Data");
        }

        Teacher teacher = teacherRepository.findById(requestDto.getTeacherId()).get();
        List<Record> records = recordRepository.findAllByTeacherAndCreateDateBetweenAndStatus(teacher, requestDto.getFrom(), requestDto.getTo(), Status.ACTIVE.getValue());

        if (records == null) {
            throw new NotFoundException("Not found");
        }
        Set<ClassDto> classDtos = new HashSet<>();
        for (Record rec : records) {
            ClassDto cl = ClassDto.builder()
                    .className(rec.getClassName2().getName())
                    .build();
            classDtos.add(cl);
        }

        response = new ResponseEntity<>(classDtos, HttpStatus.OK);

        return response;
    }

    public ResponseEntity<Set<TeacherDto>> getTeachersByDateAndClass(RequestDto requestDto) {
        ResponseEntity<Set<TeacherDto>> response = null;
        if (requestDto.getFrom() == null || requestDto.getTo() == null || requestDto.getClassId() == null) {
            throw new InvalidRequestException("Invalid Request Data");
        }

        Classes cl = classRepository.findById(requestDto.getClassId()).orElseThrow(NotFoundException::new);
        log.info("Inside service method");
        List<Record> records = recordRepository.findAllByClassName2AndCreateDateBetweenAndStatus(cl, requestDto.getFrom(), requestDto.getTo(), Status.ACTIVE.getValue());
//        System.out.println(records);
        if (records.isEmpty()) {
            throw new NotFoundException("Record Not found");
        }
        Set<TeacherDto> teacherDtos = new HashSet<>();

        for (Record rec : records) {
            TeacherDto cl1 = TeacherDto.builder()
                    .name(rec.getTeacher().getName())
                    .surName(rec.getTeacher().getSurname())
                    .build();
            teacherDtos.add(cl1);
        }

        response = new ResponseEntity<>(teacherDtos, HttpStatus.OK);

        return response;
    }

    public ResponseEntity<LessonCountDto> getLessonCountByDateAndTeacher(RequestDto requestDto) {

        ResponseEntity<LessonCountDto> response = null;
        if (requestDto.getFrom() == null || requestDto.getTo() == null || requestDto.getTeacherId() == null) {
            throw new InvalidRequestException("Invalid Request Data");
        }

        Teacher teacher = teacherRepository.findById(requestDto.getTeacherId()).get();
        List<Record> records = recordRepository.findAllByTeacherAndCreateDateBetweenAndStatus(teacher, requestDto.getFrom(), requestDto.getTo(), Status.ACTIVE.getValue());

        if (records == null) {
            throw new NotFoundException("Not found");
        }

        LessonCountDto respClassDto = LessonCountDto.builder()
                .numberOfLessons(records.size())
                .build();
        response = new ResponseEntity<>(respClassDto, HttpStatus.OK);

        return response;
    }

    public ResponseEntity<Set<LessonDto>> getLessonsByDateAndClass(RequestDto requestDto) {

        ResponseEntity<Set<LessonDto>> response = null;
        if (requestDto.getFrom() == null || requestDto.getTo() == null || requestDto.getClassId() == null) {
            throw new InvalidRequestException("Invalid Request Data");
        }

        Classes cl = classRepository.findById(requestDto.getClassId()).get();
        List<Record> records = recordRepository.findAllByClassName2AndCreateDateBetweenAndStatus(cl, requestDto.getFrom(), requestDto.getTo(), Status.ACTIVE.getValue());

        if (records == null) {
            throw new NotFoundException("Not found");
        }
        Set<LessonDto> lessonDtos = new HashSet<>();

        for (Record rec : records) {
            LessonDto cl1 = LessonDto.builder()
                    .lessonName(rec.getLesson().getName())
                    .build();
            lessonDtos.add(cl1);
        }


        response = new ResponseEntity<>(lessonDtos, HttpStatus.OK);

        return response;
    }

    public ResponseEntity<Set<RoomDto>> getRoomsByDateAndTeacher(RequestDto requestDto) {
        ResponseEntity<Set<RoomDto>> response = null;
        if (requestDto.getFrom() == null || requestDto.getTo() == null || requestDto.getTeacherId() == null) {
            throw new InvalidRequestException("Invalid Request Data");
        }

        Teacher teacher = teacherRepository.findById(requestDto.getTeacherId()).get();
        List<Record> records = recordRepository.findAllByTeacherAndCreateDateBetweenAndStatus(teacher, requestDto.getFrom(), requestDto.getTo(), Status.ACTIVE.getValue());

        if (records == null) {
            throw new NotFoundException("Not found");
        }
        Set<RoomDto> roomDtos = new HashSet<>();
        for (Record rec : records) {
            RoomDto rd = RoomDto.builder()
                    .roomNumber(rec.getRoomNumber())
                    .build();
            roomDtos.add(rd);
        }

        response = new ResponseEntity<>(roomDtos, HttpStatus.OK);

        return response;
    }
}
