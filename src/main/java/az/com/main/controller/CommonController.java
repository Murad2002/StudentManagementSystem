package az.com.main.controller;

import az.com.main.dto.*;
import az.com.main.service.CommonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/common")
@RequiredArgsConstructor
public class CommonController {
    private final CommonService commonService;

    @GetMapping("/classes")
    public ResponseEntity<Set<ClassDto>> getClassesByDateAndTeacher(@RequestBody RequestDto requestDto){
           return commonService.getClassesByDateAndTeacher(requestDto);
    }

    @GetMapping("/teachers")
    public ResponseEntity<Set<TeacherDto>> getTeachersByDateAndClass(@RequestBody RequestDto requestDto){
        return commonService.getTeachersByDateAndClass(requestDto);
    }

    @GetMapping("/lessoncount")
    public ResponseEntity<LessonCountDto> getLessonCountByDateAndTeacher(@RequestBody RequestDto requestDto){
        return commonService.getLessonCountByDateAndTeacher(requestDto);
    }

    @GetMapping("/lessons")
    public ResponseEntity<Set<LessonDto>> getLessonsByDateAndClass(@RequestBody RequestDto requestDto){
        return commonService.getLessonsByDateAndClass(requestDto);
    }

    @GetMapping("/rooms")
    public ResponseEntity<Set<RoomDto>> getRoomsByDateAndTeacher(@RequestBody RequestDto requestDto){
        return commonService.getRoomsByDateAndTeacher(requestDto);
    }

}
