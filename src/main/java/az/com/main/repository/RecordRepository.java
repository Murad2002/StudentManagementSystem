package az.com.main.repository;

import az.com.main.domain.Classes;
import az.com.main.domain.Record;
import az.com.main.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {

    List<Record> findAllByTeacherAndCreateDateBetweenAndStatus(Teacher teacher, LocalDate from, LocalDate to, Integer status);
    @Query(value = "select r.teacher,r.lesson,r.className2 from Record r where r.createDate between :from AND :to AND r.className2 = :classes AND r.status = :status")
    List<Record> findAllByClassName2AndCreateDateBetweenAndStatus(Classes classes, LocalDate from, LocalDate to, Integer status);
}
