package az.com.main.repository;

import az.com.main.domain.Classes;
import az.com.main.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends JpaRepository<Classes,Long> {
}
