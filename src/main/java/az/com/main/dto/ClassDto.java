package az.com.main.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClassDto {

    String className;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassDto classDto = (ClassDto) o;
        return Objects.equals(className, classDto.className);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className);
    }
}
