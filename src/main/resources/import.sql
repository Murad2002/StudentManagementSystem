INSERT INTO `teacher` (NAME, SURNAME, AGE, CREATE_DATE) VALUES ('Murad', 'Qadirov', 20, CURRENT_DATE);
INSERT INTO `teacher` (NAME, SURNAME, AGE, CREATE_DATE) VALUES ('Ceyhun', 'Abbasov', 20, CURRENT_DATE);

INSERT INTO `lesson` (NAME, CREATE_DATE) VALUES ('Math', CURRENT_DATE);
INSERT INTO `lesson` (NAME, CREATE_DATE) VALUES ('Physic', CURRENT_DATE);
INSERT INTO `lesson` (NAME, CREATE_DATE) VALUES ('History', CURRENT_DATE);

INSERT INTO `classes` (NAME, CREATE_DATE) VALUES ('9A', CURRENT_DATE);
INSERT INTO `classes` (NAME, CREATE_DATE) VALUES ('9B', CURRENT_DATE);

INSERT INTO `record` (teacher_id, lesson_id, class_id, CREATE_DATE) VALUES (1, 2, 1, DATE '2022-07-10');
INSERT INTO `record` (teacher_id, lesson_id, class_id, CREATE_DATE) VALUES (1, 1, 2, DATE '2022-07-12');
INSERT INTO `record` (teacher_id, lesson_id, class_id, CREATE_DATE) VALUES (2, 3, 2, DATE '2022-07-14');
INSERT INTO `record` (teacher_id, lesson_id, class_id, CREATE_DATE) VALUES (1, 3 ,2, DATE '2022-07-16');
INSERT INTO `record` (teacher_id, lesson_id, class_id, CREATE_DATE) VALUES (2, 2, 2, DATE '2022-07-18');